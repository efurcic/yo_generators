'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    this.log('###################  INITIALIZING  #################');
    this.log(yosay(
      'Welcome to the awesome ' + chalk.red('Themify') + ' generator!'
    ));

    //Store any configuration if needed?
    // this.config.set({
    //   root: 'test'
    // })

    this.config.save();
  },
  prompting: function () {
    this.log('###################  PROMPTING  #################');
    var done = this.async();

    var defaultQuestions = [
      {
        type: 'input',
        name: 'themeName',
        message: 'The name of the theme is?',
        default: this.appname
      },
      {
        type: 'list',
        name: 'platform',
        message: 'For which CMS are you building this theme?',
        choices: [
          {
            name: "None",
            value: "none",
            checked: true
          },
          {
            name: "Liferay",
            value: "liferay"
          },
          {
            name: "Drupal",
            value: "drupal"
          },
        ]
      },
      {
        type: 'checkbox',
        name: 'libraries',
        message: 'Which libraries you want to include?',
        choices: [
          {
            name: "jQuery",
            value: "jquery"
          },
          {
            name: "Normalize",
            value: "normalize"
          },
          {
            name: "Susy grid",
            value: "susy"
          },
          {
            name: "Bootstrap SASS",
            value: "bootstrap"
          },
        ]
      }
    ]

    //TODO: Expand the questionlist with CMS specific questions
    // var liferayQuestions = [
    //   {
    //     when: function (response) {
    //       if(response['platform'] == "liferay"){
    //         return response['platform'];
    //       }
    //     },
    //     type: 'input',
    //     name: 'liferayCssPath',
    //     message: 'Your path to css in webapps folder?',
    //     default: ''
    //   }
    // ];
    //
    // var drupalQuestions = [
    //   {
    //     when: function (response) {
    //       if(response['platform'] == "drupal"){
    //         return response['platform'];
    //       }
    //     },
    //     type: 'input',
    //     name: 'liferayCssPath',
    //     message: 'This is for drupal',
    //     default: ''
    //   },
    //   {
    //     when: function (response) {
    //       if(response['platform'] == "drupal"){
    //         return response['platform'];
    //       }
    //     },
    //     type: 'input',
    //     name: 'test',
    //     message: 'Test',
    //     default: 'None'
    //   }
    // ];

    //Ask all the questions
    // var questions = defaultQuestions.concat(liferayQuestions, drupalQuestions);
    var questions = defaultQuestions;

    this.prompt(questions, function (props) {
      //Save answers to this generator object
      this.appname = props.themeName;
      this.platform = props.platform;
      this.liferayCss = props.liferayCssPath;

      this.log("Theme name: " + this.appname);
      this.log("CMS name: " + this.platform);

      var features = props.libraries;

      //Go throught feature list and check if exists
      function hasFeature(feat) {
        return features && features.indexOf(feat) !== -1;
      }

      this.useJQuery = hasFeature('jquery');
      this.useNormalize = hasFeature('normalize');
      this.useSusy = hasFeature('susy');
      this.useBootstrap = hasFeature('bootstrap');


      done();
    }.bind(this));
  },
  configuring: function (){
    this.log('###################  CONFIGURING  #################');
  },
  writing: function () {
    var libraries = {
      title: this.appname,
      usejQuery : this.useJQuery,
      useNormalize : this.useNormalize,
      useSusy : this.useSusy,
      useBootstrap : this.useBootstrap
    };

    this.log('###################  WRITING  #################');
    this.log("sourceRoot: " + this.sourceRoot());
    this.log("destinationRoot: " + this.destinationRoot());

    //Create Standard Theme Folders
    this.directory('fonts', 'fonts');
    this.directory('images', 'images');

    //Create Styles
    this.directory('css', 'css');
    this.directory('sass', 'sass');
    this.copy('config.rb', 'config.rb');

    //Create Javascript
    this.directory('js', 'js');

    //Create Templates
    this.template('templates', 'templates', libraries);

    //Create Package Managers
    this.template('bower.json', 'bower.json', libraries);
    // this.template('Gemfile', 'Gemfile', libraries);
    this.copy('.bowerrc', '.bowerrc');




    //Write Drupal Stuff
    if(this.platform == "Drupal"){

    }

    //Write Liferay Stuff
    // if(this.platform == "liferay"){
    //   this.template('../liferay/config-inject.rb', 'config-inject.rb', { liferayCssPath : this.liferayCss});
    //
    // }
  },
  conflicts: function () {
    this.log('###################  CONFLICTS  #################');
  },
  install: function () {
    this.log('###################  INSTALLING  #################');
    // this.installDependencies();
    this.bowerInstall();
    this.spawnCommand('bundle', ['install']);
  }
});
