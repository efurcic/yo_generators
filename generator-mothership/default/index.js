'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var _ = require('lodash');
_.str = require('underscore.string');

// Mix in non-conflicting functions to Underscore namespace and
// Generators.
//
// Examples
//
//    this._.humanize('stuff-dash')
//    this._.classify('hello-model');
//
_.mixin(_.str.exports());

var mothershipGenerator = module.exports = function mothershipGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);

  this.themeName = this.env.themeName || 'nameless';
  this.themeDesc = this.env.themeDesc || 'nameless theme description';
  this.themeMachineName = this.env.themeMachineName || 'nameless';

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(mothershipGenerator, yeoman.generators.Base);

mothershipGenerator.prototype.askFor = function askFor() {
  // var cb = this.async();


  // var prompts = [
  //   {
  //     name: 'drupalVersion',
  //     message: 'Which Drupal version?',
  //     type: 'list',
  //     choices: [
  //       {
  //         name: 'Drupal 7',
  //         value: 'd7'
  //       },
  //       {
  //         name: 'Drupal 8',
  //         value: 'd8'
  //       }
  //     ]
  //   },
  //   {
  //     name: 'themeStyles',
  //     message: 'Add Sass, or Compass or NO css preprocessor support',
  //     type: 'list',
  //     choices: [{
  //       name: 'Compass',
  //       value: 'c'
  //     }, {
  //       name: 'Plain CSS',
  //       value: 'n'
  //     }]
  //   },
  //   {
  //     type: 'checkbox',
  //     name: 'features',
  //     message: 'What more would you like?',
  //     choices: [{
  //       name: 'Bootstrap for Sass/Compass',
  //       value: 'compassBootstrap',
  //       checked: false
  //     }]
  //   }
  // ];

  // this.prompt(prompts, function (props) {

  //   var features = props.features;
  //   this.styleSASS = (props.themeStyles === 's') ? true : false;
  //   this.styleCOMPASS = (props.themeStyles === 'c') ? true : false;
  //   this.drupalVersion = props.drupalVersion;
  //   this.compassBootstrap = features.indexOf('compassBootstrap') !== -1;
  //   this.frondly = features.indexOf('frondly') !== -1;

  //   cb();
  // }.bind(this));
};

// mothershipGenerator.prototype.app = function app() {
//   this.mkdir('js');
//   this.mkdir('css');
//   this.mkdir('images');
//   if (this.styleSASS || this.styleCOMPASS || this.compassBootstrap || this.frondly) {
//     this.mkdir('sass');
//   }
// };

// mothershipGenerator.prototype.themeStyles = function themeStyles() {
//   // this.template('_style.css', 'css/style.css');
//   // this.template('_editor.css', 'css/editor.css');
//   // if (this.styleCOMPASS || this.compassBootstrap || this.frondly) {
//   //   this.template('_config.rb', 'config.rb');
//   // }
//   // if (this.styleSASS || this.styleCOMPASS || this.compassBootstrap || this.frondly) {
//   //   this.template('__config.scss', 'sass/_config.scss');
//   //   this.template('_editor.scss', 'sass/editor.scss');
//   //   this.template('_style.scss', 'sass/style.scss');
//   // }
// };

// mothershipGenerator.prototype.themeScripts = function themeScripts() {
//     this.directory('js', 'js');
// };

// mothershipGenerator.prototype.themeInfo = function themeInfo() {

// };

mothershipGenerator.prototype.themeBase = function themeBase() {
    this.directory('css', 'css');
    this.directory('fonts', 'fonts');
    this.directory('images', 'images');
    this.directory('js', 'js');
    this.directory('lib', 'lib');
    this.directory('sass', 'sass');
    this.directory('templates', 'templates');

    this.template('_bower.json', 'bower.json');
    this.template('_config.rb', 'config.rb');
}

// mothershipGenerator.prototype.themeTemplates = function themeTemplates() {

// };

// mothershipGenerator.prototype.themeImages = function themeImages() {
//   this.directory('images', 'images');
// };

// mothershipGenerator.prototype.bowerFiles = function bowerFiles() {
//   this.template('_bower.json', 'bower.json');
//   this.copy('bowerrc', '.bowerrc');
// };

// mothershipGenerator.prototype.packageFiles = function packageFiles() {
//   this.packageInfo = {
//     "name": this.themeMachineName,
//     "version": "0.0.0"
//   };
//   this.template('_package.json', 'package.json');
// };
