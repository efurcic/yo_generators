<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><%= title %></title>
    <% if (usejQuery) { -%>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <% } -%>
  </head>
  <body>

  </body>
</html>
